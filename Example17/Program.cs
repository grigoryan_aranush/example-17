﻿using System;
class Example
{
    static void Main()
    {
        int x;
        x = 10;
        if (x == 10) //start new scope
        {
            int y = 20; //known only in this block

            //x and y both known here.
            Console.WriteLine("{0} and {1}: ",x,y);
            x = y * 2;

        }
        //y here is not known here
        //x is still known here.
        Console.WriteLine("x is " + x);
    }
}